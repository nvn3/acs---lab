#include <cuda.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include "Timer.h"

typedef float TYPE;
const long N =  (8*1024*1024);

void cpuAdd(TYPE *A, TYPE *B, TYPE *Sum)
{
    int i;
    for (i = 0; i < N; ++i)
    {
        Sum[i] = A[i] + B[i];
    }
}

__global__ void addKernel(float *A, float *B, float *C, int n)
{
    unsigned int index = blockIdx.x * blockDim.x + threadIdx.x;
    if(index<n)
        C[index] = A[index] + B[index];
}

void gpuAdd(TYPE *A, TYPE *B, TYPE *C)
{
    dim3 threads(512);
    dim3 blocks(N/threads.x);
    addKernel<<<blocks,threads>>>(A, B, C, N);
    cudaDeviceSynchronize();
}

int main(int argc, char **argv)
{
    Timer cpuTime, gpuTime;
    initTimer(&cpuTime, "CPU Time");
    initTimer(&gpuTime, "GPU Time");

    TYPE *A;
    TYPE *B;
    TYPE *C;
    int i;

    // allocation
    cudaMallocManaged(&A, N*sizeof(TYPE));
    cudaMallocManaged(&B, N*sizeof(TYPE));
    cudaMallocManaged(&C, N*sizeof(TYPE));

    // initialization
    for (i = 0; i < N; ++i)
    {
        A[i]=1;
        B[i]=2;
    }

    // cpu computation
    startTimer(&cpuTime);
    cpuAdd(A,B,C);
    stopTimer(&cpuTime);

     // gpu computation
    startTimer(&gpuTime);
    gpuAdd(A,B,C);
    stopTimer(&gpuTime);

    // free
    cudaFree(A);
    cudaFree(B);
    cudaFree(C);

    printTimer(cpuTime);
    printTimer(gpuTime);

    return 0;
}

