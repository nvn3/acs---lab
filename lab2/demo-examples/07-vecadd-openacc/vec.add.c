#include <stdio.h>
#include <stdlib.h>
#include "Timer.h"

typedef float TYPE;
const long N =  (8*1024*1024);

void cpu();
void gpu1();
void gpu2();

int main (int argc)
{
    cpu();
    gpu1();
    gpu2();
    return 0;
}

void cpu()
{
    printf("\nCPU run\n");
    Timer timer;
    initTimer(&timer, "CPU Time");
    TYPE *Arr1 = malloc( N*sizeof(TYPE) );
    TYPE *Arr2 = malloc( N*sizeof(TYPE) );
    TYPE *Sum  = malloc( N*sizeof(TYPE) );
    int i;

    startTimer(&timer);
    for (i = 0; i < N; i ++)
    {
        Arr1[i] = 1;
        Arr2[i] = 2;
    }

    for (i=0; i<N; i++)
    {
        Sum[i] = Arr1[i] + Arr2[i];
    }
    stopTimer(&timer);

    printTimer(&timer);
    printf("Sum : %g\n", Sum[0]);
    free(Arr1);
    free(Arr2);
    free(Sum);
}

void gpu1()
{
    printf("\nGPU1 run\n");
    Timer timer;
    initTimer(&timer, "GPU1 Time");
    TYPE *Arr1 = malloc( N*sizeof(TYPE) );
    TYPE *Arr2 = malloc( N*sizeof(TYPE) );
    TYPE *Sum  = malloc( N*sizeof(TYPE) );
    int i;

    startTimer(&timer);
    #pragma acc kernels loop copy(Arr1[0:N], Arr2[0:N])
    for (i = 0; i < N; i ++)
    {
        Arr1[i] = 1;
        Arr2[i] = 2;
    }

    #pragma acc kernels loop copy(Arr1[0:N], Arr2[0:N], Sum[0:N])
    for (i=0; i<N; i++)
    {
        Sum[i] = Arr1[i] + Arr2[i];
    }
    stopTimer(&timer);

    printTimer(&timer);
    printf("Sum : %g\n", Sum[0]);
    free(Arr1);
    free(Arr2);
    free(Sum);
}

void gpu2()
{
    printf("\nGPU2 run\n");
    Timer timer;
    initTimer(&timer, "GPU2 Time");
    TYPE * restrict Arr1 = malloc( N*sizeof(TYPE) );
    TYPE * restrict Arr2 = malloc( N*sizeof(TYPE) );
    TYPE * restrict Sum  = malloc( N*sizeof(TYPE) );
    int i;

    startTimer(&timer);
    #pragma acc data create(Arr1[0:N], Arr2[0:N]) copyout(Sum[0:N])
    {
        #pragma acc kernels loop
        for (i = 0; i < N; i ++)
        {
            Arr1[i] = 1;
            Arr2[i] = 2;
        }

        #pragma acc kernels loop
        for (i=0; i<N; i++)
        {
            Sum[i] = Arr1[i] + Arr2[i];
        }
    }
    stopTimer(&timer);

    printTimer(&timer);
    printf("Sum : %g\n", Sum[0]);
    free(Arr1);
    free(Arr2);
    free(Sum);
}

