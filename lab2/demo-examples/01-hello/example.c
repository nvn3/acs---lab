#include <stdio.h>

void times2_kernel(int *a, int *b) 
{
    int i;
    for(i=0; i<1024; i++)
        b[i] = 2*a[i];
}

int main() 
{
    printf("CPU TIMES2\n");
    int i, in[1024], out[1024];   // create arrays on CPU

    // Initialise the input in_dta on the CPU.
    for (i = 0; i<1024; ++i)
        in[i] = i;

    times2_kernel(in, out);

    printf("in  out\n");
    for (i=0; i<=5; ++i)
        printf("%d  %d\n", in[i], out[i]);

    return 0;
}
