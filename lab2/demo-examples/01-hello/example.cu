#include <stdio.h>

__global__ void times2_kernel(int *a, int *b) 
{
    int i = blockIdx.x;
    b[i] = 2*a[i];
}

int main() 
{
    printf("GPU TIMES2\n");
    int in_h[1024], out_h[1024];   // create arrays on CPU
    int *in_d, *out_d;       // pointers to arrays on GPU

    cudaMalloc((void **)&in_d, 1024*sizeof(int));
    cudaMalloc((void **)&out_d, 1024*sizeof(int));

    // Initialise the input in_dta on the CPU.
    for (int i = 0; i<1024; ++i)
        in_h[i] = i;

    // Copy input in_dta to array on GPU.
    cudaMemcpy(in_d, in_h, 1024*sizeof(int), cudaMemcpyHostToDevice);

    // Launch GPU code with 1024 threads, one per array element.
    times2_kernel<<<1024, 1>>>(in_d, out_d);

    // Copy output array from GPU back to CPU.
    cudaMemcpy(out_h, out_d, 1024*sizeof(int), cudaMemcpyDeviceToHost);

    printf("in  out\n");
    for (int i=0; i<=5; ++i)
        printf("%d  %d\n", in_h[i], out_h[i]);

    // Free up the arrays on the GPU.
    cudaFree(in_d);
    cudaFree(out_d);

    return 0;
}
