#include <stdio.h>


__device__ int x;

__global__ void unaligned_kernel(void) 
{
    *(int*) ((char*)&x + 1) = 42;
}
__global__ void out_of_bounds_kernel(void) 
{
    *(int*) 0x87654320 = 42;
}

__global__ void set_kernel(int * dmem) 
{
    *dmem = 1;
}

void run_set(int * dmem)
{
    printf("Running set_kernel\n");
    set_kernel<<<1,1>>>(dmem);
    printf("Ran set_kernel: %s\n",
    cudaGetErrorString(cudaGetLastError()));
    printf("Sync: %s\n", cudaGetErrorString(cudaThreadSynchronize()));
}

void run_unaligned()
{
    printf("Running unaligned_kernel\n");
    unaligned_kernel<<<1,1>>>();
    printf("Ran unaligned_kernel: %s\n",
    cudaGetErrorString(cudaGetLastError()));
    printf("Sync: %s\n", cudaGetErrorString(cudaThreadSynchronize()));
}

void run_out_of_bounds()
{
    printf("Running out_of_bounds_kernel\n");
    out_of_bounds_kernel<<<1,1>>>();
    printf("Ran out_of_bounds_kernel: %s\n",
    cudaGetErrorString(cudaGetLastError()));
    printf("Sync: %s\n", cudaGetErrorString(cudaThreadSynchronize()));
}

int main() 
{
    int *dmem;

    printf("Mallocing memory\n");
    cudaMalloc((void**)&dmem, 1024);

    run_set(dmem);
    run_unaligned();
    run_out_of_bounds();

    cudaDeviceReset();

    return 0;
}
