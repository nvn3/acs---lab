#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <iostream>
#include "Timer.hpp"

typedef float TYPE;
const int N = 8 * 1024 * 1024;

using namespace std;
using LOFAR::NSTimer;

static void checkCudaCall(cudaError_t result)
{
    if (result != cudaSuccess)
    {
        cerr << "cuda error: " << cudaGetErrorString(result) << endl;
        exit(1);
    }
}

__global__ void vectorAddKernel(TYPE* deviceA, TYPE* deviceB, TYPE* deviceResult)
{
    unsigned index = blockIdx.x * blockDim.x + threadIdx.x;

    deviceResult[index] = deviceA[index] + deviceB[index];
}


void vectorAddCuda(int n, TYPE* a, TYPE* b, TYPE* result)
{
    int threadBlockSize = 512;

    // allocate the vectors on the GPU
    TYPE* deviceA = NULL;
    checkCudaCall(cudaMalloc((void **) &deviceA, n * sizeof(TYPE)));
    if (deviceA == NULL)
    {
        cout << "could not allocate memory!" << endl;
        return;
    }
    TYPE* deviceB = NULL;
    checkCudaCall(cudaMalloc((void **) &deviceB, n * sizeof(TYPE)));
    if (deviceB == NULL)
    {
        checkCudaCall(cudaFree(deviceA));
        cout << "could not allocate memory!" << endl;
        return;
    }
    TYPE* deviceResult = NULL;
    checkCudaCall(cudaMalloc((void **) &deviceResult, n * sizeof(TYPE)));
    if (deviceResult == NULL)
    {
        checkCudaCall(cudaFree(deviceA));
        checkCudaCall(cudaFree(deviceB));
        cout << "could not allocate memory!" << endl;
        return;
    }

    NSTimer kernelTime = NSTimer("kernelTime", false, false);
    NSTimer memoryTime = NSTimer("memoryTime", false, false);

    // copy the original vectors to the GPU
    memoryTime.start();
    checkCudaCall(cudaMemcpy(deviceA, a, n*sizeof(TYPE), cudaMemcpyHostToDevice));
    checkCudaCall(cudaMemcpy(deviceB, b, n*sizeof(TYPE), cudaMemcpyHostToDevice));
    memoryTime.stop();

    // execute kernel
    kernelTime.start();
    vectorAddKernel<<<n/threadBlockSize, threadBlockSize>>>(deviceA, deviceB, deviceResult);
    cudaDeviceSynchronize();
    kernelTime.stop();

    // check whether the kernel invocation was successful
    checkCudaCall(cudaGetLastError());

    // copy result back
    memoryTime.start();
    checkCudaCall(cudaMemcpy(result, deviceResult, n * sizeof(TYPE), cudaMemcpyDeviceToHost));
    memoryTime.stop();

    checkCudaCall(cudaFree(deviceA));
    checkCudaCall(cudaFree(deviceB));
    checkCudaCall(cudaFree(deviceResult));

    cout << fixed << setprecision(6);
    cout << "vector-add (kernel): \t\t" << kernelTime.getElapsed()/1000.0 << " msec." << endl;
    cout << "vector-add (memory): \t\t" << memoryTime.getElapsed()/1000.0 << " msec." << endl;
    cout << "vector-add (GPU): \t\t" << (kernelTime.getElapsed() + memoryTime.getElapsed())/1000.0 << " msec." << endl;
}


int main(int argc, char* argv[])
{
    NSTimer CPUTime = NSTimer("CPUTime", false, false);
    TYPE* a = new TYPE[N];
    TYPE* b = new TYPE[N];
    TYPE* result = new TYPE[N];
    TYPE* resultCpu = new TYPE[N];

    CPUTime.start();
    for(int i=0; i<N; i++)
    {
        a[i] = 1;
        b[i] = 2;
    }

    for(int i=0; i<N; i++)
    {
        resultCpu[i] = a[i] + b[i] ;
    }
    CPUTime.stop();
    cout << fixed << setprecision(6);
    cout << "vector-add (CPU): \t\t" << CPUTime.getElapsed()/1000.0 << " msec." << endl;

    vectorAddCuda(N, a, b, result);

    delete[] a;
    delete[] b;
    delete[] result;

    return 0;
}
