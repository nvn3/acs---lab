// Copyright 2018 Delft University of Technology
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <iostream>
#include <memory>
#include "../baseline/water.hpp"
#include "../utils/Histogram.hpp"

#include "../baseline/imgproc.hpp"

#include "water_cuda.hpp"
#include <cmath>
#include <vector>
#include <algorithm>

__global__ void histogramCuda(const Pixel *pixels, int* d_histogram, int width, int height)
{
  int channel;

  uint i = (blockIdx.x * blockDim.x) + threadIdx.x;
  uint j = (blockIdx.y * blockDim.y) + threadIdx.y;
  uint k = j*width + i;

  Pixel pixel_unit = pixels[k];
  if (k < width*height)
  {
    for (channel = 0; channel < 4; channel++)
      atomicAdd(&d_histogram[channel*256 + pixel_unit.colors[channel]], 1);
  }
}

std::shared_ptr<Image> runWaterEffectCUDA(const Image *src, const WaterEffectOptions *options)
{
  int totalImageSize = src->height * src->width * sizeof(Pixel);
  const size_t size_of_hist = sizeof(int)*4*256;
  int *d_histogram, *h_histogram; // one for device and other for the host
  Pixel *d_pixels;
  int numBlocksHeight = ceil(src->height / 16.0);
  int numBlocksWidth = ceil(src->width / 16.0);

  // Histogram elements
  cudaMallocManaged((void**)& d_histogram, size_of_hist); // 4 channels
  cudaMemset(d_histogram, 0, size_of_hist);
  h_histogram = (int*)malloc(size_of_hist);

  // Image pixels for device
  cudaMallocManaged((void**)&d_pixels, totalImageSize);
  cudaMemcpy(d_pixels, src->pixels, totalImageSize, cudaMemcpyHostToDevice); // transfer the image to the device

  const dim3 blockSize( numBlocksWidth, numBlocksHeight, 1);
  const dim3 gridSize( 16, 16, 1);

  histogramCuda<<< blockSize, gridSize >>>(d_pixels, d_histogram, src->width, src->height);

  cudaMemcpy(h_histogram, d_histogram, size_of_hist, cudaMemcpyDeviceToHost);
  cudaDeviceSynchronize();

  Histogram *hist = new Histogram();
  memcpy(hist->values.data(), h_histogram, size_of_hist);

  std::shared_ptr<Histogram> hist_sp = std::make_shared<Histogram>(*hist);
  std::shared_ptr<Image> img_ret = hist_sp->toImage();

  cudaFree(d_histogram);
  cudaFree(d_pixels);
  free(hist);
  free(h_histogram);

  return img_ret;
}
