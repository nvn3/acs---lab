import numpy as np
import matplotlib.pyplot as plt

data_matrix = np.genfromtxt('mat.csv',delimiter=',', usecols=np.arange(0,6)) # Get data from csv file baseline matrix multiplication

data_omp = np.genfromtxt('mat_omp.csv',delimiter=',', usecols=np.arange(0,6)) # Get data from csv file openmp

matrix_size = [column[1] for column in data_matrix]    # Matrix Dimensions
matrix_size.remove(matrix_size[0]);                            # Set to 0 since top element is text
mat_dim = list(map(int,matrix_size))            # Change list elements to integer values

matrix_float = [column[4] for column in data_matrix]              # Time values of float vector calculations
matrix_double = [column[5] for column in data_matrix]              # Time values of double vector calculations

matrix_float.remove(matrix_float[0]);   # Remove the Column Title
matrix_double.remove(matrix_double[0]); # --||--

omp_float = [column[4] for column in data_omp]
omp_double = [column[5] for column in data_omp]
omp_float.remove(omp_float[0])
omp_double.remove(omp_double[0])

ind = np.arange(len(matrix_size))  # The x locations for the groups

width = 0.25  # Set the width of the bars for the graph

#### Bar graph Float

fig, bar_graph = plt.subplots()

plt.figure(1) 

# Plot bars on the graph figure
rect1 = bar_graph.bar(ind - width/2, matrix_float, width, color='#00a30d', label='Matrix Baseline')
rect2 = bar_graph.bar(ind + width/2, omp_float, width, color='#ce0a48', label='OpenMP')

# Add labels, title etc.
bar_graph.set_ylabel('time [s]')
bar_graph.set_xlabel('Matrix Dimensions [N x N]')
bar_graph.set_title('OpenMP vs Matrix Baseline Float')
bar_graph.set_xticks(ind)
bar_graph.set_xticklabels(mat_dim)
bar_graph.legend(loc='upper left')


#### Bar graph Double

fig, bar_graph = plt.subplots()

plt.figure(2) 

# Plot bars on the graph figure
rect1 = bar_graph.bar(ind - width/2, matrix_double, width, color='#00a30d', label='Matrix Baseline')
rect2 = bar_graph.bar(ind + width/2, omp_double, width, color='#ce0a48', label='OpenMP')

# Add labels, title etc.
bar_graph.set_ylabel('time [s]')
bar_graph.set_xlabel('Matrix Dimensions [N x N]')
bar_graph.set_title('OpenMP vs Matrix Baseline Double')
bar_graph.set_xticks(ind)
bar_graph.set_xticklabels(mat_dim)
bar_graph.legend(loc='upper left')

#### Line graph Float

fig, line_graph = plt.subplots()

plt.figure(3)
plt.plot(ind,matrix_float, label='Matrix Baseline', linestyle='--',marker='*')
plt.plot(ind,omp_float, label='OpenMP', marker='*')
line_graph.set_ylabel('time[s]')
line_graph.set_xlabel('Matrix Dimensions [N x N]')
line_graph.set_title('OpenMP vs Matrix Baseline Float')
line_graph.set_xticks(ind)
line_graph.set_xticklabels(mat_dim)
line_graph.legend(loc='upper left')


#### Line graph Double

fig, line_graph_double = plt.subplots()

plt.figure(4)
plt.plot(ind,matrix_double, label='Matrix Baseline', linestyle='--',marker='*')
plt.plot(ind,omp_double, label='OpenMP', marker='*')
line_graph_double.set_ylabel('time[s]')
line_graph_double.set_xlabel('Matrix Dimensions [N x N]')
line_graph_double.set_title('OpenMP vs Matrix Baseline Double')
line_graph_double.set_xticks(ind)
line_graph_double.set_xticklabels(mat_dim)
line_graph_double.legend(loc='upper left')

plt.show()