// Copyright 2018 Delft University of Technology
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// OpenMP:
#include <omp.h>

#include "../matmult.hpp"

/* You may not remove these pragmas: */
/*************************************/
#pragma GCC push_options
#pragma GCC optimize ("O1")
/*************************************/

Matrix<float> multiplyMatricesOMP(Matrix<float> a,
                                  Matrix<float> b,
                                  int num_threads) {
  omp_set_num_threads(num_threads);
  int arows = a.rows;
  int acol = a.columns;
  int dim = a.rows;   // Get the number of rows/cols
  int bcol = b.columns;
  auto mat_f = Matrix<float>(a.rows,b.columns);  // Create a double matrix to store solution
  double tmp = 0.0;
  int count = 0;

#pragma omp parallel        // Use OpenMP for the calculations
  {
    int i,j,k;
#pragma omp for// schedule(static) reduction(+:tmp)
    for(i=0; i<arows; i++)  // Matrix Calculations
    {
      for(j=0; j<bcol; j++)

      {
        for(k=0; k<acol; k++)
        {
//#pragma omp atomic
          tmp += a(i,k)*b(k,j);
        }
        mat_f(i,j) = tmp;
        tmp = 0;
      }
      count = count + 1;
    }
  }
  /* Debug statements for checking the output
  a.print();
  b.print();
  mat_f.print();
  */
  // printf("\nOpenMP Single Precision\n");
  // mat_f.print();
  return mat_f;
}

Matrix<double> multiplyMatricesOMP(Matrix<double> a,
                                   Matrix<double> b,
                                   int num_threads) {
  omp_set_num_threads(num_threads);
  int arows = a.rows;
  int acol = a.columns;
  int dim = a.rows;   // Get the number of rows/cols
  int bcol = b.columns;
  auto mat_f = Matrix<double>(a.rows,b.columns);  // Create a double matrix to store solution
  double tmp = 0.0;
  int count = 0;

#pragma omp parallel        // Use OpenMP for the calculations
  {
    int i,j,k;
#pragma omp for// schedule(static) reduction(+:tmp)
    for(i=0; i<arows; i++)  // Matrix Calculations
    {
      for(j=0; j<bcol; j++)

      {
        for(k=0; k<acol; k++)
        {
//#pragma omp atomic
          tmp += a(i,k)*b(k,j);
        }
        mat_f(i,j) = tmp;
        tmp = 0;
      }
      count = count + 1;
    }
  }
  /* Debug statements for checking the output
  a.print();
  b.print();
  mat_f.print();
  */
  // printf("\nOpenMP Double Precision\n");
  // mat_f.print();
  return mat_f;
}
#pragma GCC pop_options
