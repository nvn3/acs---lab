// Copyright 2018 Delft University of Technology
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "../matmult.hpp"

// Intel intrinsics for SSE/AVX:
#include <immintrin.h>

#include "../utils/Timer.hpp"

#include<bits/stdc++.h>

/* You may not remove these pragmas: */
/*************************************/
#pragma GCC push_options
#pragma GCC optimize ("O1")
/*************************************/

typedef union _avxd {
  __m256d val;
  double arr[4];
} avxd;

typedef union _avxf {
  __m256 val;
  float arr[8];
} avxf;

// for FLOAT
inline float accumulate(__m256 x, int size = 0)
{
  float *s;
    __m128 l = _mm256_extractf128_ps(x, 0); //implicit
    __m128 h = _mm256_extractf128_ps(x, 1);
    l = _mm_add_ps(l, h);
    l = _mm_hadd_ps(l, l);
    s = (float*)&l;
    return s[0] + s[1];
}

// for DOUBLE
inline double accumulate(__m256d x, int size = 0)
{
  __m256d x1 = _mm256_hadd_pd(x,x);
  __m256d x2 = _mm256_hadd_pd(x1,x1);

  double *s;
  // __m128d l = _mm256_extractf128_pd(x, 0); //implicit
  // __m128d h = _mm256_extractf128_pd(x, 1);
  // l = _mm_add_pd(l, h);
  s = (double*)&x2;
  // return s[0] + s[1];
  return s[0];
}

void printElements(__m256 s)
{
  float *temp = (float*)&s;

  for (int i = 0; i < 8; i++) std::cout << temp[i] << ' ';
  std::cout << std::endl;
}

float sum_and_product(float* x, float* y, size_t elements)
{
  float acc_res(0.0);
  _avxf vec_a, vec_b;

  vec_a.val = _mm256_setzero_ps(); //Initialize vector to zero
  vec_b.val = _mm256_setzero_ps(); //Initialize vector to zero

  vec_a.val = _mm256_loadu_ps(x);
  vec_b.val = _mm256_loadu_ps(y);
  vec_a.val = _mm256_mul_ps(vec_a.val, vec_b.val);

  for(int i = 0; i < elements; i++)
    acc_res += vec_a.arr[i];
  return acc_res;
}


double sum_and_product(double* x, double* y, size_t elements)
{
  double acc_res(0.0);
  _avxd vec_a, vec_b;

  vec_a.val = _mm256_setzero_pd(); //Initialize vector to zero
  vec_b.val = _mm256_setzero_pd(); //Initialize vector to zero

  vec_a.val = _mm256_loadu_pd(x);
  vec_b.val = _mm256_loadu_pd(y);

  vec_a.val = _mm256_mul_pd(vec_a.val, vec_b.val);
  for(int i = 0; i < elements; i++)
    acc_res += vec_a.arr[i];
  return acc_res;
}


Matrix<double> multiplyMatricesSIMD(Matrix<double> a, Matrix<double> b)
{
  if (a.columns != b.rows)   // Test if matrices can be multiplied.
    throw std::domain_error("Matrix dimensions do not allow matrix-multiplication.");

  // Height and width
  auto rows = a.rows;
  auto columns = b.columns;
  const size_t SIZE = 4;

  // Create the resulting matrix
  auto result = Matrix<double>(rows, columns);

  size_t additional = a.columns % SIZE;
  size_t pass_required = a.columns / SIZE;
  size_t passes;

  // Outer loop
  for(int i = 0; i < a.rows; i++)
  {
    for(int j = 0; j < b.columns; j++)
    {
      passes = 0;
      // b(j,i)
      double buff_a[4], buff_b[4];
      double res(0.0);

      while (passes != pass_required)
      {
        double *a_ptr = &a(i,passes*4); // verification needed
        
        memset((void*)buff_b, 0, sizeof(double)*4);

        int p = 0;
        for(int x = passes * 4; x < (passes*4)+4; x++)
        {
          buff_b[p] = b(x,j);
          p++;
        }

        res += sum_and_product(a_ptr, buff_b, 4);
        passes++;
      }
      if (additional > 0)  // process additional items here
      {
        double *a_ptr = &a(i,passes*4);

        memset((void*)buff_b, 0, sizeof(double)*4);

        int p = 0;
        for(int x = passes * 4; x < (passes*4)+additional; x++)
        {
          buff_b[p] = b(x,j);
          p++;
        }
        res += sum_and_product(a_ptr, buff_b, additional);
      }

      result(i,j) = res;
    }
  }
  return result;
}

Matrix<float> multiplyMatricesSIMD(Matrix<float> a, Matrix<float> b)
{
  if (a.columns != b.rows)   // Test if matrices can be multiplied.
    throw std::domain_error("Matrix dimensions do not allow matrix-multiplication.");

  // Height and width
  auto rows = a.rows;
  auto columns = b.columns;
  const size_t SIZE = 8;

  // Create the resulting matrix
  auto result = Matrix<float>(rows, columns);

  size_t additional = a.columns % SIZE;
  size_t pass_required = a.columns / SIZE;
  size_t passes;

  // Outer loop
  for(int i = 0; i < a.rows; i++)
  {
    for(int j = 0; j < b.columns; j++)
    {
      passes = 0;
      // b(j,i)
      float buff_a[8], buff_b[8];
      float res(0.0);

      while (passes != pass_required)
      {
        float *a_ptr = &a(i,passes*8); // verification needed
        
        memset((void*)buff_b, 0, sizeof(float)*8);

        int p = 0;
        for(int x = passes * 8; x < (passes*8)+8; x++)
        {
          buff_b[p] = b(x,j);
          p++;
        }

        res += sum_and_product(a_ptr, buff_b, 8);
        passes++;
      }
      if (additional > 0)  // process additional items here
      {
        float *a_ptr = &a(i,passes*8);

        memset((void*)buff_b, 0, sizeof(float)*8);

        int p = 0;
        for(int x = passes * 8; x < (passes*8)+additional; x++)
        {
          buff_b[p] = b(x,j);
          p++;
        }
        res += sum_and_product(a_ptr, buff_b, additional);
      }

      result(i,j) = res;
    }
  }
  return result;
}

/*************************************/
#pragma GCC pop_options
/*************************************/
