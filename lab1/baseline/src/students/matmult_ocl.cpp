// Copyright 2018 Delft University of Technology
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// OpenCL
#ifdef __APPLE__
#include "OpenCL/opencl.h"
#else
#include "CL/cl.h"
#endif

#include "../matmult.hpp"

/* You may not remove these pragmas: */
/*************************************/
#pragma GCC push_options
#pragma GCC optimize ("O1")

/*************************************/

// Macro to check clFunction outputs.
// Throw an error if not successful, to make debugging easier.
#define CHECK(err) if (err != CL_SUCCESS) { \
  throw std::runtime_error("OpenCL error: " + std::to_string(err) + \
  " in " + __FILE__ + " line " + std::to_string(__LINE__) ); \
}

///@brief A little enum class to help us parse clDeviceInfo
enum class ClInfoType {
  CHAR, SIZE_T, //... add your own info types
};


/// @brief Function to discover OpenCL devices and print some info on stdout.
static std::vector<cl_device_id> discoverDevices(cl_platform_id platform_id) {
  std::vector<cl_device_id> devices;
  // Discover devices for each platform
  cl_uint num_devices = 0;
  // Get number of devices of this type, we will only discover GPU devices for now.
  // https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/clGetDeviceIDs.html
  int err = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_GPU, 0, nullptr, &num_devices);

  // std::cout << "\tDevices: " << num_devices << std::endl;

  if ((err != CL_DEVICE_NOT_FOUND) || (num_devices != 0)) {
    // Get the devices of this type and insert them into the final list
    std::vector<cl_device_id> platform_type_devices(num_devices);
    CHECK(clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_ALL, num_devices, platform_type_devices.data(), &num_devices));
    // Insert the found devices into the final result
    devices.insert(std::end(devices), std::begin(platform_type_devices), std::end(platform_type_devices));

    // Many infos exist for devices. Also see:
    // https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/clGetDeviceInfo.html
    //
    // DISCLAIMER: IT IS HIGHLY RECOMMENDED TO DISCOVER SOME MORE STUFF ABOUT YOUR DEVICE WHEN YOU ARE GOING TO
    // USE IT MORE INTENSELY

    for (auto platform_type_device : platform_type_devices) {
      std::vector<cl_device_info> info_queries = {CL_DEVICE_NAME, CL_DEVICE_MAX_WORK_GROUP_SIZE};
      std::vector<ClInfoType> info_types = {ClInfoType::CHAR, ClInfoType::SIZE_T};
      size_t info_size = 0;
      for (unsigned int i = 0; i < info_queries.size(); i++) {
        // Get the query size
        CHECK(clGetDeviceInfo(platform_type_device, info_queries[i], 0, nullptr, &info_size));
        auto query = new char[info_size];
        CHECK(clGetDeviceInfo(platform_type_device, info_queries[i], info_size, query, &info_size));
        switch (info_types[i]) {
          case ClInfoType::SIZE_T: //std::cout << *(size_t*)query << std::endl;
            break;
          default://std::cout << query << std::endl;
            break;
        }
        delete[] query;

      }
    }
  }
  return devices;
}

/// @brief Function to discover OpenCL platforms and print some info on stdout.
static std::vector<cl_platform_id> discoverPlatforms() {
  cl_uint num_platforms = 0;

  // Obtain the number of OpenCL platforms
  // https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/clGetPlatformIDs.html
  CHECK(clGetPlatformIDs(0, nullptr, &num_platforms));

  // OpenCL sometimes outputs some stuff on cerr. Flush this stuff from the stream.
  std::cerr.flush();

  // std::cout << "Found " << num_platforms << " OpenCL platform(s)." << std::endl;

  // Create an array to hold platform IDs.
  auto platform_ids = std::vector<cl_platform_id>(num_platforms);

  // Get the actual platform IDs
  CHECK(clGetPlatformIDs(num_platforms, platform_ids.data(), &num_platforms));

  // Identify the platform info that we would like to discover (more infos exist, but they are not interesting for us)
  const std::vector<cl_platform_info> platform_queries = {CL_PLATFORM_NAME, CL_PLATFORM_VENDOR, CL_PLATFORM_VERSION};

  // Iterate over all platforms
  for (unsigned int p = 0; p < num_platforms; p++) {
    // std::cout << "Platform " << p << std::endl;

    // Iterate over all platform infos we want to inquire
    for (auto platform_query : platform_queries) {
      size_t query_size = 0;

      // Get the current platform info length
      CHECK(clGetPlatformInfo(platform_ids[p], platform_query, 0, nullptr, &query_size));
      auto query = new char[query_size];

      // Get the actual info
      CHECK(clGetPlatformInfo(platform_ids[p], platform_query, query_size, query, &query_size));

      // std::cout << '\t' << query << std::endl;

      delete[] query;
    }
  }

  return platform_ids;
}

Matrix<float> multiplyMatricesOCL(Matrix<float> a,
                                  Matrix<float> b) {

  float* A = a.values.get();
  float* B = b.values.get();

  int M, K, N;
  M = a.rows;
  N = b.columns;
  K = b.rows;

  Matrix<float> c(M,N);
  float *C = c.values.get();


  // Create a little variable to store OpenCL error codes.
  int err;

  // First, we must discover all available OpenCL platforms
  auto platforms = discoverPlatforms();

  // If there are any platforms
  if (platforms.empty()) {
      throw std::runtime_error("No OpenCL platforms detected.");
  }

  // Discover the devices on the first platform.
  // Running on the cluster node should give you only one platform.
  // Be aware that your local setup might be different.
  auto devices = discoverDevices(platforms[0]);

  // If there are any devices
  if (devices.empty()) {
      throw std::runtime_error("No OpenCL devices detected.");
  }

  // Create an OpenCL context.
  // https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/clCreateContext.html
  // We will let the implementation of this function automatically select the platform, so
  // the first argument is a nullptr. We will use the first device only. so the number of devices is 1. You should
  // not ever diverge from this during the lab. We will not use a callback function with any user data, so the next two
  // arguments are both nullptrs. Finally we let the function return any error code into err.
  auto context = clCreateContext(nullptr, 1, &devices[0], nullptr, nullptr, &err);

  // Create an OpenCL program from some source that does element wise vector multiplication.
  // https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/clCreateProgramWithSource.html
  // Obviously, it would be prettier if this came from some file.

  //auto kernel_src = util::loadProgram("sxy_kernel.cl");

  auto kernel_src = "__kernel void naive(__global float* A,                     \n" \
                    "                    __global float* B,                     \n" \
                    "                    __global float* C,                     \n" \
                    "                    const int M,                           \n" \
                    "                    const int N,                           \n" \
                    "                    const int K)                           \n" \
                    "{                                                          \n" \
                    "                                                           \n" \
                    "       int row = get_global_id(0);                         \n" \
                    "       int col = get_global_id(1);                         \n" \
                    "                                                           \n" \
                    "      float acc = 0.0f;                                    \n" \
                    "       if(row > M || col > N){return;}                     \n" \
                    "       for (int k = 0; k < K; ++k){                        \n" \
                    "                                                           \n" \
                    "               acc += A[k + K * col] * B[N * k + row];     \n" \
                    "       }                                                   \n" \
                    "                                                           \n" \
                    "       C[col * M + row] = acc;                             \n" \
                    "                                                           \n" \
                    "}                                                          \n" \
                    "\n";

  auto program = clCreateProgramWithSource(context, 1, (const char **) &kernel_src, nullptr, &err);

  // And build the program
  clBuildProgram(program, 0, nullptr, nullptr, nullptr, nullptr);

  // Create a command queue
  auto queue = clCreateCommandQueue(context, devices[0], 0, &err);

  // Create the OpenCL kernel "object" that can be sent to the device.
  auto kernel = clCreateKernel(program, "naive", &err);

  // std::cout << kernel;

  // Create some buffers on the device to hold the data
  auto devbuf_A = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(float)*M*K, nullptr, nullptr);
  auto devbuf_B = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(float)*K*N, nullptr, nullptr);

  // And one output buffer
  auto devbuf_C = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(float)*M*N, nullptr, nullptr);

  // Enqueue transfers to write to the buffers.
  CHECK(clEnqueueWriteBuffer(queue, devbuf_A, CL_TRUE, 0, sizeof(float)*M*K, &A[0], 0, nullptr, nullptr));
  CHECK(clEnqueueWriteBuffer(queue, devbuf_B, CL_TRUE, 0, sizeof(float)*K*N, &B[0], 0, nullptr, nullptr));

  // Ready the kernel for computation. We must first set its arguments
  CHECK(clSetKernelArg(kernel, 0, sizeof(cl_mem), &devbuf_A));
  CHECK(clSetKernelArg(kernel, 1, sizeof(cl_mem), &devbuf_B));
  CHECK(clSetKernelArg(kernel, 2, sizeof(cl_mem), &devbuf_C));
  CHECK(clSetKernelArg(kernel, 3, sizeof(int), &M));
  CHECK(clSetKernelArg(kernel, 4, sizeof(int), &N));
  CHECK(clSetKernelArg(kernel, 5, sizeof(int), &K));

  int ls;
  if(M<32){
  ls = M;
  } else {
  ls = 32;
  }
 const int TS = ls;
 const size_t local[2] = {(size_t)TS, (size_t)TS};
 const size_t global[2] = {(size_t)M, (size_t)N};

  // The number of items to work on in a local in every local work group.
  //    size_t local_size = 2;
  // The number of items to work on globally. Make sure that this is an integer multiple of local_size.
  //    size_t global_size = 2;

  // Enqueue the execution of the kernel.
  // DISCLAIMER: IF SO FAR YOU HAVE NOT READ ANY API DOCUMENTATION, THIS IS A GREAT TIME TO START DOING SO
  // THIS IS AN IMPORTANT FUNCTION AND ITS PARAMETERS WILL INFLUENCE THE PERFORMANCE OF YOUR IMPLEMENTATION GREATLY.
  // https://www.khronos.org/registry/OpenCL/sdk/1.0/docs/man/xhtml/clEnqueueNDRangeKernel.html
  CHECK(clEnqueueNDRangeKernel(queue, kernel, 2, nullptr, global, local, 0, nullptr, nullptr));

  // Wait for the queue to finish...
  clFinish(queue);

  // Read the results back, from device to host
  // https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/clEnqueueReadBuffer.html
  CHECK(clEnqueueReadBuffer(queue, devbuf_C, CL_TRUE, 0, sizeof(float)*M*N, &C[0], 0, nullptr, nullptr));

  // Clean up all the resources we've used.
  clReleaseMemObject(devbuf_A);
  clReleaseMemObject(devbuf_B);
  clReleaseMemObject(devbuf_C);
  clReleaseProgram(program);
  clReleaseKernel(kernel);
  clReleaseCommandQueue(queue);
  clReleaseContext(context);

  return c;
}

Matrix<double> multiplyMatricesOCL(Matrix<double> a,
                                   Matrix<double> b) {

  double* A = a.values.get();
  double* B = b.values.get();

  int M, K, N;
  M = a.rows;
  N = b.columns;
  K = b.rows;

  Matrix<double> c(M,N);

  double *C = c.values.get();


  // Create a little variable to store OpenCL error codes.
  int err;

  // First, we must discover all available OpenCL platforms
  auto platforms = discoverPlatforms();

  // If there are any platforms
  if (platforms.empty()) {
     throw std::runtime_error("No OpenCL platforms detected.");
  }

  // Discover the devices on the first platform.
  // Running on the cluster node should give you only one platform.
  // Be aware that your local setup might be different.
  auto devices = discoverDevices(platforms[0]);

  // If there are any devices
  if (devices.empty()) {
     throw std::runtime_error("No OpenCL devices detected.");
  }

  // Create an OpenCL context.
  // https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/clCreateContext.html
  // We will let the implementation of this function automatically select the platform, so
  // the first argument is a nullptr. We will use the first device only. so the number of devices is 1. You should
  // not ever diverge from this during the lab. We will not use a callback function with any user data, so the next two
  // arguments are both nullptrs. Finally we let the function return any error code into err.
  auto context = clCreateContext(nullptr, 1, &devices[0], nullptr, nullptr, &err);

  // Create an OpenCL program from some source that does element wise vector multiplication.
  // https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/clCreateProgramWithSource.html
  // Obviously, it would be prettier if this came from some file.

  //auto kernel_src = util::loadProgram("sxy_kernel.cl");

  auto kernel_src = "__kernel void naive(__global double* A,                     \n" \
                    "                    __global double* B,                     \n" \
                    "                    __global double* C,                     \n" \
                    "                    const int M,                           \n" \
                    "                    const int N,                           \n" \
                    "                    const int K)                           \n" \
                    "{                                                          \n" \
                    "                                                           \n" \
                    "       int row = get_global_id(0);                         \n" \
                    "       int col = get_global_id(1);                         \n" \
                    "                                                           \n" \
                    "      double acc = 0.0;                                    \n" \
                    "       if(row > M || col > N){return;}                     \n" \
                    "       for (int k = 0; k < K; ++k){                        \n" \
                    "                                                           \n" \
                    "               acc += A[k + K * col] * B[N * k + row];     \n" \
                    "       }                                                   \n" \
                    "                                                           \n" \
                    "       C[col * M + row] = acc;                             \n" \
                    "                                                           \n" \
                    "}                                                          \n" \
                    "\n";

  auto program = clCreateProgramWithSource(context, 1, (const char **) &kernel_src, nullptr, &err);

  // And build the program
  clBuildProgram(program, 0, nullptr, nullptr, nullptr, nullptr);

  // Create a command queue
  auto queue = clCreateCommandQueue(context, devices[0], 0, &err);

  // Create the OpenCL kernel "object" that can be sent to the device.
  auto kernel = clCreateKernel(program, "naive", &err);

  std::cout << kernel;

  // Create some buffers on the device to hold the data
  auto devbuf_A = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(double)*M*K, nullptr, nullptr);
  auto devbuf_B = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(double)*K*N, nullptr, nullptr);

  // And one output buffer
  auto devbuf_C = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(double)*M*N, nullptr, nullptr);

  // Enqueue transfers to write to the buffers.
  CHECK(clEnqueueWriteBuffer(queue, devbuf_A, CL_TRUE, 0, sizeof(double)*M*K, &A[0], 0, nullptr, nullptr));
  CHECK(clEnqueueWriteBuffer(queue, devbuf_B, CL_TRUE, 0, sizeof(double)*K*N, &B[0], 0, nullptr, nullptr));

  // Ready the kernel for computation. We must first set its arguments
  CHECK(clSetKernelArg(kernel, 0, sizeof(cl_mem), &devbuf_A));
  CHECK(clSetKernelArg(kernel, 1, sizeof(cl_mem), &devbuf_B));
  CHECK(clSetKernelArg(kernel, 2, sizeof(cl_mem), &devbuf_C));
  CHECK(clSetKernelArg(kernel, 3, sizeof(int), &M));
  CHECK(clSetKernelArg(kernel, 4, sizeof(int), &N));
  CHECK(clSetKernelArg(kernel, 5, sizeof(int), &K));

  int ls;
  if(M<32){
  ls = M;
  } else {
  ls = 32;
  }
 const int TS = ls;
 const size_t local[2] = {(size_t)TS, (size_t)TS};
 const size_t global[2] = {(size_t)M, (size_t)N};

  // The number of items to work on in a local in every local work group.
  //    size_t local_size = 2;
  // The number of items to work on globally. Make sure that this is an integer multiple of local_size.
  //    size_t global_size = 2;

  // Enqueue the execution of the kernel.
  // DISCLAIMER: IF SO FAR YOU HAVE NOT READ ANY API DOCUMENTATION, THIS IS A GREAT TIME TO START DOING SO
  // THIS IS AN IMPORTANT FUNCTION AND ITS PARAMETERS WILL INFLUENCE THE PERFORMANCE OF YOUR IMPLEMENTATION GREATLY.
  // https://www.khronos.org/registry/OpenCL/sdk/1.0/docs/man/xhtml/clEnqueueNDRangeKernel.html
  CHECK(clEnqueueNDRangeKernel(queue, kernel, 2, nullptr, global, local, 0, nullptr, nullptr));

  // Wait for the queue to finish...
  clFinish(queue);

  // Read the results back, from device to host
  // https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/clEnqueueReadBuffer.html
  CHECK(clEnqueueReadBuffer(queue, devbuf_C, CL_TRUE, 0, sizeof(double)*M*N, &C[0], 0, nullptr, nullptr));

  // Clean up all the resources we've used.
  clReleaseMemObject(devbuf_A);
  clReleaseMemObject(devbuf_B);
  clReleaseMemObject(devbuf_C);
  clReleaseProgram(program);
  clReleaseKernel(kernel);
  clReleaseCommandQueue(queue);
  clReleaseContext(context);

  return c;
}

/*************************************/
#pragma GCC pop_options
/*************************************/
