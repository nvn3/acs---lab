#include <immintrin.h>
#include <iostream>

using namespace std;

int main()
{
  __m256 a1;
  __m256 a2;

  float a[] = {1,2,3,4,5,6,7,8,9};
  float b[] = {1,2,3,4,5,6,7,8,9};

  a1 = _mm256_loadu_ps(&a[1]);
  a2 = _mm256_loadu_ps(b);

  for (int i = 0; i < 8; i++)
  {
    cout << "this:" << ((float*)&a1)[i] << endl;
  }

}
