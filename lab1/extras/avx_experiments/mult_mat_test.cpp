#include <iostream>
#include <immintrin.h>
#include "Matrix.hpp"
#include "Timer.hpp"
#include <vector>
#include <string>

using namespace std;

float accumulate(__m256 vec, int num)
{
  float *ret = (float*)&vec;
  float result = 0.0;
  for (int j = 0; j < num; j++) result += ret[j];
  return result;
}

Matrix<float> multiply(Matrix<float> &a, Matrix<float> &b) {
  cout << "Multiplying using the normal method:" << endl;
  // Test if matrices can be multiplied.
  if (a.columns != b.rows) {
    throw std::domain_error("Matrix dimensions do not allow matrix-multiplication.");
  }

  // Height and width
  auto rows = a.rows;
  auto columns = b.columns;

  // Create the resulting matrix
  auto result = Matrix<float>(rows, columns);

  // For each row
  for (size_t r = 0; r < rows; r++) {
    // For each column
    for (size_t c = 0; c < columns; c++) {

      float e = 0.0;
      // Accumulate the product of the elements
      for (size_t i = 0; i < b.rows; i++) {
        e += a(r, i) * b(i, c);
      }

      // Store the result
      result(r, c) = e;
    }
  }

  return result;
}

Matrix<float> myMultiply(Matrix<float> &a, Matrix<float> &b) {
  cout << "Multiplying using the AVX method:" << endl;
  __m256 vec_a;
  __m256 vec_b;
  float a_arr[] = {0, 0, 0, 0, 0, 0, 0, 0};
  float b_arr[] = {0, 0, 0, 0, 0, 0, 0, 0};

  // Test if matrices can be multiplied.
  if (a.columns != b.rows) {
    throw std::domain_error("Matrix dimensions do not allow matrix-multiplication.");
  }

  // Height and width
  auto rows = a.rows;
  auto columns = b.columns;

  // Create the resulting matrix
  auto result = Matrix<float>(rows, columns);

  // For each row
  for (size_t r = 0; r < rows; r++) {
    // For each column
    for (size_t c = 0; c < columns; c++) {

      int e = 0;
      // Accumulate the product of the elements
      for (size_t i = 0; i < b.rows; i++) {
        a_arr[i] = a(r,i);
        b_arr[i] = b(i, c);
      }

      vec_a = _mm256_loadu_ps(a_arr);
      vec_b = _mm256_loadu_ps(b_arr);

      float result_acc = accumulate(_mm256_mul_ps(vec_a,vec_b), b.rows);
      // cout << "result:" << result_acc << endl;
      // Store the result
      result(r, c) = result_acc;
    }
  }
  return result;
}

int main(int argc, char *argv[])
{
  // -my for AVX implementation else normal method
  cout << argv[1] << endl;
  Timer tick;
  Matrix<float> m1(8,8), m2(8,8);

  m1.values.get()[0] = 1.0;
  m1.values.get()[1] = 1.0;
  m1.values.get()[2] = 1.0;
  m1.values.get()[3] = 1.0;
  m1.values.get()[4] = 1.0;
  m1.values.get()[5] = 1.0;
  m1.values.get()[6] = 1.0;
  m1.values.get()[7] = 1.0;
  m1.values.get()[8] = 1.0;
  m1.values.get()[9] = 1.0;
  m1.values.get()[10] = 1.0;
  m1.values.get()[11] = 1.0;
  m1.values.get()[12] = 1.0;
  m1.values.get()[13] = 1.0;
  m1.values.get()[14] = 1.0;
  m1.values.get()[15] = 1.0;
  m1.values.get()[16] = 1.0;
  m1.values.get()[17] = 1.0;
  m1.values.get()[18] = 1.0;
  m1.values.get()[19] = 1.0;
  m1.values.get()[20] = 1.0;
  m1.values.get()[21] = 1.0;
  m1.values.get()[22] = 1.0;
  m1.values.get()[23] = 1.0;
  m1.values.get()[24] = 1.0;
  m1.values.get()[25] = 1.0;
  m1.values.get()[26] = 1.0;
  m1.values.get()[27] = 1.0;
  m1.values.get()[28] = 1.0;
  m1.values.get()[29] = 1.0;
  m1.values.get()[30] = 1.0;
  m1.values.get()[31] = 1.0;
  m1.values.get()[32] = 1.0;
  m1.values.get()[33] = 1.0;
  m1.values.get()[34] = 1.0;
  m1.values.get()[35] = 1.0;
  m1.values.get()[36] = 1.0;
  m1.values.get()[37] = 1.0;
  m1.values.get()[38] = 1.0;
  m1.values.get()[39] = 1.0;
  m1.values.get()[40] = 1.0;
  m1.values.get()[41] = 1.0;
  m1.values.get()[42] = 1.0;
  m1.values.get()[43] = 1.0;
  m1.values.get()[44] = 1.0;
  m1.values.get()[45] = 1.0;
  m1.values.get()[46] = 1.0;
  m1.values.get()[47] = 1.0;
  m1.values.get()[48] = 1.0;
  m1.values.get()[49] = 1.0;
  m1.values.get()[50] = 1.0;
  m1.values.get()[51] = 1.0;
  m1.values.get()[52] = 1.0;
  m1.values.get()[53] = 1.0;
  m1.values.get()[54] = 1.0;
  m1.values.get()[55] = 1.0;
  m1.values.get()[56] = 1.0;
  m1.values.get()[57] = 1.0;
  m1.values.get()[58] = 1.0;
  m1.values.get()[59] = 1.0;
  m1.values.get()[60] = 1.0;
  m1.values.get()[61] = 1.0;
  m1.values.get()[62] = 1.0;
  m1.values.get()[63] = 1.0;


  m2.values.get()[0] = 1.0;
  m2.values.get()[1] = 1.0;
  m2.values.get()[2] = 1.0;
  m2.values.get()[3] = 1.0;
  m2.values.get()[4] = 1.0;
  m2.values.get()[5] = 1.0;
  m2.values.get()[6] = 1.0;
  m2.values.get()[7] = 1.0;
  m2.values.get()[8] = 1.0;
  m2.values.get()[9] = 1.0;
  m2.values.get()[10] = 1.0;
  m2.values.get()[11] = 1.0;
  m2.values.get()[12] = 1.0;
  m2.values.get()[13] = 1.0;
  m2.values.get()[14] = 1.0;
  m2.values.get()[15] = 1.0;
  m2.values.get()[16] = 1.0;
  m2.values.get()[17] = 1.0;
  m2.values.get()[18] = 1.0;
  m2.values.get()[19] = 1.0;
  m2.values.get()[20] = 1.0;
  m2.values.get()[21] = 1.0;
  m2.values.get()[22] = 1.0;
  m2.values.get()[23] = 1.0;
  m2.values.get()[24] = 1.0;
  m2.values.get()[25] = 1.0;
  m2.values.get()[26] = 1.0;
  m2.values.get()[27] = 1.0;
  m2.values.get()[28] = 1.0;
  m2.values.get()[29] = 1.0;
  m2.values.get()[30] = 1.0;
  m2.values.get()[31] = 1.0;
  m2.values.get()[32] = 1.0;
  m2.values.get()[33] = 1.0;
  m2.values.get()[34] = 1.0;
  m2.values.get()[35] = 1.0;
  m2.values.get()[36] = 1.0;
  m2.values.get()[37] = 1.0;
  m2.values.get()[38] = 1.0;
  m2.values.get()[39] = 1.0;
  m2.values.get()[40] = 1.0;
  m2.values.get()[41] = 1.0;
  m2.values.get()[42] = 1.0;
  m2.values.get()[43] = 1.0;
  m2.values.get()[44] = 1.0;
  m2.values.get()[45] = 1.0;
  m2.values.get()[46] = 1.0;
  m2.values.get()[47] = 1.0;
  m2.values.get()[48] = 1.0;
  m2.values.get()[49] = 1.0;
  m2.values.get()[50] = 1.0;
  m2.values.get()[51] = 1.0;
  m2.values.get()[52] = 1.0;
  m2.values.get()[53] = 1.0;
  m2.values.get()[54] = 1.0;
  m2.values.get()[55] = 1.0;
  m2.values.get()[56] = 1.0;
  m2.values.get()[57] = 1.0;
  m2.values.get()[58] = 1.0;
  m2.values.get()[59] = 1.0;
  m2.values.get()[60] = 1.0;
  m2.values.get()[61] = 1.0;
  m2.values.get()[62] = 1.0;
  m2.values.get()[63] = 1.0;

  tick.start();
  Matrix<float> res(8,8);
  if (strcmp(argv[1],"-my") == 0) res = myMultiply(m1,m2);
  else res = multiply(m1,m2);
  tick.stop();

  cout << "Time elapsed: " << tick.seconds() << endl;
  for(int i = 0; i < 64; i++) cout << res.values.get()[i] << ' ';
  cout << endl;
}
