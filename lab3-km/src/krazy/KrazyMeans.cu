// Copyright 2018 Delft University of Technology
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <fstream>
#include "KrazyMeans.hpp"
#include "../utils/Timer.hpp"
#include <cmath>


// KERNEL FUNCTIONS START HERE

/* Args. need: -> dataElements,     -> numberOfDataElements, -> clusterElements,
               -> numberOfClusters, -> featureSize,          -> labels
               -> iteration,        -> scale_factor,         -> scale_threshold_iterations*/
__global__ void CUDA_updateLabels(float* dataElements, size_t numberOfDataElements,
                                  float* clustElements, size_t numberOfClusters,
                                  size_t featureSize, uint* labels,
                                  uint iteration, float scale_factor,
                                  uint scale_threshold_iterations)
{
  uint k = blockIdx.x * 1024 + threadIdx.x;

  if(k < numberOfDataElements)
  {
    uint *update = &labels[numberOfDataElements];
    float dist, factor = 1.0f, closest = 80000.0;

    size_t closest_cluster = 0;

    if (iteration >= scale_threshold_iterations) factor = 1.0f + (float) (iteration - scale_threshold_iterations) * scale_factor;

    for(int i = 0; i < numberOfClusters; i++)
    {
      dist = 0.0f;
      for (int feature = 0; feature < featureSize; feature++)
        dist += ( (dataElements[k*featureSize + feature] - clustElements[i*featureSize + feature]) * (dataElements[k*featureSize + feature] - clustElements[i*featureSize + feature]) );
      dist = sqrtf(dist);

      if (labels[k] != i) dist *= factor;

      if (dist < closest)
      {
        closest = dist; closest_cluster = i;
      }
    }

    // updation of the label here
    if (labels[k] != closest_cluster)
    {
      labels[k] = closest_cluster;
      atomicAdd(update, 1);
    }
  }
}

/* Args. need: -> dataElements,     -> numberOfDataElements, -> clustElements,
               -> numberOfClusters, -> featureSize,          -> labels
               -> assignments*/
__global__ void CUDA_accumulate_features(float* dataElements, size_t numberOfDataElements,
                                         float* clustElements, size_t numberOfClusters,
                                         size_t featureSize, uint* labels, uint *assignments)
{
  uint i = blockIdx.x * 1024 + threadIdx.x;

  if(i < numberOfDataElements)
  {
    // printf("hw %i\n",i);
    atomicAdd(&assignments[labels[i]],1);
    for(int feature = 0; feature < featureSize; feature++)
    {
      atomicAdd(&clustElements[labels[i] * featureSize + feature],dataElements[i * featureSize + feature]);
    }
  }
}

// KERNEL FUNCTIONS END HERE

KrazyMeans::KrazyMeans(const std::shared_ptr<DataSet> &data_set,
                       unsigned int num_clusters,
                       unsigned int scale_threshold_iters,
                       float scale_factor)
    : data_set(data_set),
      num_clusters(num_clusters),
      scale_factor(scale_factor),
      scale_threshold_iterations(scale_threshold_iters)
{
  // Initialize all labels to 0
  labels.reserve(data_set->vectors.size()+1);
  for (size_t i = 0; i < data_set->vectors.size(); i++) {
    labels.push_back(0);
  }
  std::cout << "Size of data set is " << data_set->vectors.size() << std::endl;
  totalFloats = data_set->vectors.size() * data_set->num_features;
  // Initialize the centroids to be all zero.
  centroids = std::vector<FeatureVec>(num_clusters, FeatureVec(data_set->num_features, 0.0f));
  Linearize_DataSet();
}

// Converts the native centroids to linear centroids vector.
void KrazyMeans::Linearize_Centroids(std::vector<float>& dst_cluster)
{
  std::vector<FeatureVec>::iterator fv_it;
  std::vector<float>::iterator elem_it;

  for(fv_it = centroids.begin(); fv_it != centroids.end(); fv_it++)
  {
    for(elem_it = fv_it->values.begin(); elem_it != fv_it->values.end(); elem_it++)
      dst_cluster.push_back(*elem_it); // inserting every element into the linear vector
  }
}

// Converts the linear centroids vector to native centroids.
void KrazyMeans::Delinearize_Centroids(std::vector<float>& dst_cluster)
{
  std::vector<FeatureVec>::iterator fv_it;
  std::vector<float>::iterator elem_it;

  int count = 0;
  for(fv_it = centroids.begin(); fv_it != centroids.end(); fv_it++)
  {
    for(elem_it = fv_it->values.begin(); elem_it != fv_it->values.end(); elem_it++)
      *elem_it = dst_cluster[count++];
  }
}

// toDev - true : host->dev, false : dev->host
inline void KrazyMeans::copyLabels(bool toDev)
{
  float *pinLabels;
  if(toDev)
  {
    cudaMallocHost((void **) &pinLabels, (data_set->vectors.size()+1) * sizeof(uint));
    memcpy(pinLabels, labels.data(), (data_set->vectors.size()+1) * sizeof(uint));
    cudaMemcpy(dev_Labels, pinLabels, (data_set->vectors.size()+1) * sizeof(uint), cudaMemcpyHostToDevice);
  }
  else
  {
    cudaMallocHost((void **) &pinLabels, (data_set->vectors.size()+1) * sizeof(uint));
    cudaMemcpy(pinLabels, dev_Labels, (data_set->vectors.size()+1) * sizeof(uint), cudaMemcpyDeviceToHost);
    memcpy(labels.data(), pinLabels, (data_set->vectors.size()+1) * sizeof(uint));
  }
  cudaFreeHost(pinLabels);
}

// To update the labels of the datasets, output needed labels.
bool KrazyMeans::GPU_updateLabels()
{
  bool updated = false;
  const uint numThreads = 1024;
  uint blockSize = ceil(data_set->vectors.size() / float(numThreads));
  int totCentroidSize = centroids.size() * data_set->num_features * sizeof(float);

  std::vector<float> temp_centroids;
  Linearize_Centroids(temp_centroids);
  
  cudaMemcpy(dev_Centroids, temp_centroids.data(), totCentroidSize, cudaMemcpyHostToDevice);

  labels[data_set->vectors.size()] = 0; // making the updated to zero.
  copyLabels(true); // true host->device copy
  // cudaMemcpy(dev_Labels, labels.data(), (data_set->vectors.size()+1) * sizeof(uint), cudaMemcpyHostToDevice);

  CUDA_updateLabels <<< blockSize, numThreads >>> \\
  (dev_Dataset, data_set->vectors.size(), dev_Centroids, num_clusters, \\
   data_set->num_features, dev_Labels, iteration, scale_factor, scale_threshold_iterations);
  cudaDeviceSynchronize();
  
  // cudaMemcpy(labels.data(), dev_Labels, (data_set->vectors.size() + 1)* sizeof(uint), cudaMemcpyDeviceToHost);
  copyLabels(false); // Move back the updated labels which were update by the GPU

  updated = labels[data_set->vectors.size()] > 0 ? true : false;

  return updated;
}

void KrazyMeans::GPU_updateCentroids()
{
  clearCentroids(); // Clear the centroids
  const uint numThreads = 1024;
  uint blockSize = ceil(data_set->vectors.size() / float(numThreads));

  std::vector<uint> assignments(centroids.size(), 0); // store number of elements are going to assigned each centroid

  std::vector<float> temp_centroids; // to store centroids in linear form
  Linearize_Centroids(temp_centroids); // TODO this is not neccessary, just init with all zeros.

  // Copying the centroid data to
  int totCentroidSize = centroids.size() * data_set->num_features * sizeof(float);
  // cudaMallocManaged((void **) &dev_Centroids, totCentroidSize);
  cudaMemcpy(dev_Centroids, temp_centroids.data(), totCentroidSize, cudaMemcpyHostToDevice);
  cudaMemcpy(dev_assignments, assignments.data(), sizeof(uint) * centroids.size(), cudaMemcpyHostToDevice);

  CUDA_accumulate_features <<< blockSize, numThreads >>> (dev_Dataset, data_set->vectors.size(), \\
  dev_Centroids, num_clusters, data_set->num_features, dev_Labels, dev_assignments);

  cudaMemcpy(temp_centroids.data(), dev_Centroids, totCentroidSize, cudaMemcpyDeviceToHost);
  cudaMemcpy(assignments.data(), dev_assignments, sizeof(uint) * centroids.size(), cudaMemcpyDeviceToHost);

  Delinearize_Centroids(temp_centroids);

  // Updated Centroids, average still needed
  for(size_t c = 0; c < centroids.size(); c ++)
  {
    if (assignments[c] > 0)
    {
      for(size_t f = 0; f < data_set->num_features; f++)
        centroids[c][f] /= (float) assignments[c];
    }
  }
}

void KrazyMeans::Linearize_DataSet()
{
  lin_values.reserve(totalFloats);

  std::vector<FeatureVec>::iterator fv_it;
  std::vector<float>::iterator elem_it;

  for(fv_it = data_set->vectors.begin(); fv_it != data_set->vectors.end(); fv_it++)
  {
    for(elem_it = fv_it->values.begin(); elem_it != fv_it->values.end(); elem_it++)
      lin_values.push_back(*elem_it); // inserting every element into the linear vector
  }
}

size_t KrazyMeans::findClosestCentroidIndex(size_t vec_index) {
  float closest = INFINITY;
  size_t index = 0;

  float factor = 1.0f;

  // When we reach the iterations threshold, penalize the distance for any vector switching to another centroid
  // This will cause faster convergence
  if (iteration >= scale_threshold_iterations) {
    factor = 1.0f + (float) (iteration - scale_threshold_iterations) * scale_factor;
  }

  for (size_t c = 0; c < centroids.size(); c++) {
    float dist = calculateEuclideanDistance(data_set->vector(vec_index), centroids[c]);

    // Penalize potential swapping of labels with a factor
    if (labels[vec_index] != c) {
      dist = dist * factor;
    }

    if (dist < closest) {
      closest = dist;
      index = c;
    }
  }
  return index;
}

void KrazyMeans::selectRandomCentroids() {
  UniformRandomGenerator<long> rg;
  // For each cluster centroid, randomly select a feature vector as initialization.
  for (auto &vector : centroids) {
    vector = data_set->vectors[rg.next() % data_set->size()];
  }
}

bool KrazyMeans::updateLabels() {
  auto updated = false;
  // For each feature vector, find the current closest centroid
  for (size_t i = 0; i < data_set->size(); i++) {
    auto closest = findClosestCentroidIndex(i);
    if (labels[i] != closest) {
      labels[i] = closest;
      updated = true;
    }
  }
  return updated;
}

void KrazyMeans::clearCentroids() {
  for (auto &centroid : centroids) {
    centroid.clear();
  }
}

void KrazyMeans::updateCentroids() {
  // Extremely naive implementation to update centroids.

  // Clear the centroids
  clearCentroids();

  // Iterate over all centroids
  for (size_t c = 0; c < centroids.size(); c++) {
    // Keep track of amount of feature vectors assigned to this centroid.
    size_t num_assigned = 0;

    // Find all vectors that belong to this centroid label
    for (size_t v = 0; v < data_set->size(); v++) {
      // If this vector is assigned to this centroid...
      if (labels[v] == c) {
        num_assigned++;
        // Accumulate the centroid feature values
        for (size_t f = 0; f < centroids[c].size(); f++) {
          centroids[c][f] += data_set->vectors[v][f];
        }
      }
    }
    // Average out each feature if new assignments were made
    if (num_assigned != 0) {
      for (size_t f = 0; f < data_set->num_features; f++) {
        centroids[c][f] /= (float) num_assigned;
      }
    }
  }
}

void KrazyMeans::MoveDStoGPU() // uses pinned memory
{
  float* pin_Dataset;
  cudaMallocHost((void **) &pin_Dataset, totalFloats * sizeof(float));
  cudaMalloc((void **) &dev_Dataset, totalFloats * sizeof(float));
  memcpy(pin_Dataset, lin_values.data(), totalFloats * sizeof(float));//, cudaMemcpyHostToDevice);
  cudaMemcpy(dev_Dataset, pin_Dataset, totalFloats * sizeof(float), cudaMemcpyHostToDevice);
  cudaFreeHost(pin_Dataset);
}

void KrazyMeans::GPU_selectRandomCentroids() {
  UniformRandomGenerator<long> rg;
  // For each cluster centroid, randomly select a feature vector as initialization.
  for (auto &vector : centroids) {
    vector = data_set->vectors[rg.next() % data_set->size()];
  }

  std::vector<float> temp_centroids; // to store centroids in linear form
  Linearize_Centroids(temp_centroids);

  // Copying the centroid data to the GPU
  int totCentroidSize = centroids.size() * data_set->num_features * sizeof(float);
  cudaMemcpy(dev_Centroids, temp_centroids.data(), totCentroidSize, cudaMemcpyHostToDevice);
}

void KrazyMeans::initialize()
{
  cudaMallocManaged((void **) &dev_Labels, (data_set->vectors.size()+1) * sizeof(uint));
  int totCentroidSize = centroids.size() * data_set->num_features * sizeof(float);
  cudaMallocManaged((void **) &dev_Centroids, totCentroidSize);
  cudaMallocManaged((void**)&dev_assignments, sizeof(uint) * centroids.size());
  MoveDStoGPU();
  GPU_selectRandomCentroids();
  GPU_updateLabels();
}

void KrazyMeans::iterate() {
  GPU_updateCentroids();
  converged = !GPU_updateLabels();
  iteration++;
}

void KrazyMeans::run(bool quiet) {
  while (!converged) {
    // Only quiet mode should be optimized. Non-quiet mode is not required.
    if (quiet) {
      // Quiet mode
      iterate();
    }
    else {
      // Non quiet mode
      Timer t;
      t.start();
      iterate();
      t.stop();
      std::cout << iteration << " - " << t.seconds() << " s." << std::endl;
    }
  }
}

void KrazyMeans::printState(std::ostream &labels_out, std::ostream &centroids_out) {
  // Print labels for all vectors
  for (size_t v = 0; v < data_set->size(); v++) {
    labels_out << v << ", " << labels[v] << ", " << data_set->vector(v).toString() << std::endl;
  }

  // Print centroids
  for (size_t c = 0; c < num_clusters; c++) {
    centroids_out << c << ", " << centroids[c].toString() << std::endl;
  }
}

void KrazyMeans::printLabels()
{
  for(int i = 0; i < labels.size(); i++) printf("%i:%i, ", i, labels[i]);
  printf("\n");
}

void KrazyMeans::dumpLabels(std::string file_name) {
  std::ofstream file(file_name, std::ofstream::out);

  // Write labels as uint16_t
  for (size_t v = 0; v < data_set->size(); v++) {
    file.write((char *) &labels[v], sizeof(size_t));
  }
}